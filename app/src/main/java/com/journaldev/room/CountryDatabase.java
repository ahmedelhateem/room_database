package com.journaldev.room;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.journaldev.room.dao.CountryDao;
import com.journaldev.room.model.CountryDataEntity;

@Database(entities = {CountryDataEntity.class}, version = 3)

public abstract class CountryDatabase extends RoomDatabase {
    public static final String DB_NAME = "country_db";
    public abstract CountryDao countryDao();
}
