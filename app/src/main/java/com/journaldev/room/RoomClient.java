package com.journaldev.room;

import android.content.Context;

import androidx.room.Room;

public class RoomClient {

    private static RoomClient mInstance;
    private Context mCtx;
    //our app database object
    private CountryDatabase appDatabase;

    private RoomClient(Context mCtx) {
        this.mCtx = mCtx;

        //creating the app database with Room database builder
        //MyToDos is the name of the database
        appDatabase = Room.databaseBuilder(mCtx, CountryDatabase.class, CountryDatabase.DB_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }

    public static synchronized RoomClient getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new RoomClient(mCtx);
        }
        return mInstance;
    }

    public CountryDatabase getAppDatabase() {
        return appDatabase;
    }
}
