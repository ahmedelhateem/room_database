package com.journaldev.room.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.journaldev.room.model.CountryDataEntity;

import java.util.List;





@Dao
public interface CountryDao {

    @Query("SELECT * FROM countryData")
    List<CountryDataEntity> getData();

    @Insert
    void insert(CountryDataEntity task);

    @Delete
    void delete(CountryDataEntity task);

    @Update
    void update(CountryDataEntity task);


}
