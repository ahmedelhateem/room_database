package com.journaldev.room.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "countryData")
public class CountryDataEntity {
    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "currency")
    private String currency;

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private  long id;

    public CountryDataEntity() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    @Ignore
    public CountryDataEntity(String name, String currency) {
        this.name = name;
        this.currency = currency;
    }
    @Ignore
    public CountryDataEntity(String name, String currency, long id) {
        this.name = name;
        this.currency = currency;
        this.id = id;
    }
}
