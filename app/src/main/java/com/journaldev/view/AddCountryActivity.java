package com.journaldev.view;

/**
 * Created by anupamchugh on 19/10/15.
 */
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.journaldev.room.RoomClient;
import com.journaldev.room.model.CountryDataEntity;
import com.journaldev.sqlite.DBManager;
import com.journaldev.sqlite.R;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class AddCountryActivity extends Activity implements OnClickListener {

    private Button addTodoBtn;
    private EditText subjectEditText;
    private EditText descEditText;

    private DBManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("Add Record");

        setContentView(R.layout.activity_add_record);

        subjectEditText =  findViewById(R.id.subject_edittext);
        descEditText =  findViewById(R.id.description_edittext);

        addTodoBtn =  findViewById(R.id.add_record);

        dbManager = new DBManager(this);
        dbManager.open();
        addTodoBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.add_record:

                final String name = subjectEditText.getText().toString();
                final String desc = descEditText.getText().toString();

           //  dbManager.insert(name, desc);
                addCountry(name,desc);
                Intent main = new Intent(AddCountryActivity.this, CountryListActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(main);
                break;
        }
    }
    @SuppressLint("CheckResult")
    public void addCountry(String name, String desc) {

        Observable.fromCallable(() -> {
            RoomClient.getInstance(this).getAppDatabase().countryDao().insert(new CountryDataEntity(name, desc));
            return true;
        }).subscribeOn(Schedulers.newThread())
                .subscribe(aBoolean -> {
                    Log.e("TAG_DATABASE", "result : " + aBoolean);
                }, throwable -> {
                    Log.e("TAG_DATABASE", "error : ", throwable);
                });



    }


}