package com.journaldev.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.journaldev.room.model.CountryDataEntity;
import com.journaldev.sqlite.R;

import org.apache.commons.collections4.CollectionUtils;

import java.util.LinkedList;
import java.util.List;


public  abstract class CountryAdapter extends RecyclerView.Adapter<CountryAdapter.ViewHolder> {

    private List<CountryDataEntity> array = new LinkedList<>();
    private Context context;

    public CountryAdapter(Context context) {
        this.context = context;
    }

    public void setArray(List<CountryDataEntity> array) {
        if (CollectionUtils.isEmpty(array)) return;
        this.array.clear();
        this.array.addAll(array);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CountryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.activity_view_record, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CountryAdapter.ViewHolder holder, int position) {
        final CountryDataEntity countryData =array.get(position);

        holder.title.setText(countryData.getName());
        holder.id.setText(String.valueOf(countryData.getId()));
        holder.describtion.setText(countryData.getCurrency());
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click(countryData,v);
            }
        });

    }


    @Override
    public int getItemCount() {
        return array.size();
    }
    public abstract void click(CountryDataEntity item, View view);

    class ViewHolder extends RecyclerView.ViewHolder {

        AppCompatTextView title,describtion,id;
        RelativeLayout container;
        ViewHolder(View itemView) {
            super(itemView);
            title=itemView.findViewById(R.id.title);
            describtion=itemView.findViewById(R.id.desc);
            container=itemView.findViewById(R.id.layout_container);
            id=itemView.findViewById(R.id.id);


        }

    }

}



