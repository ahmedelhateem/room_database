package com.journaldev.view;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cursoradapter.widget.SimpleCursorAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.journaldev.room.RoomClient;
import com.journaldev.room.model.CountryDataEntity;
import com.journaldev.sqlite.DBManager;
import com.journaldev.sqlite.DatabaseHelper;
import com.journaldev.sqlite.R;

import java.util.LinkedList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class CountryListActivity extends AppCompatActivity {

    private DBManager dbManager;

    //  private ListView listView;
    private RecyclerView recyclerView;
    private SimpleCursorAdapter adapter;
    private CountryAdapter countryAdapter;
    private Cursor cursor;
    private AppCompatTextView noRecords;
    //    final String[] from = new String[]{DatabaseHelper._ID,
//            DatabaseHelper.SUBJECT, DatabaseHelper.DESC};
//
//    final int[] to = new int[]{R.id.id, R.id.title, R.id.desc};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_emp_list);

        dbManager = new DBManager(this);
        dbManager.open();
        cursor = dbManager.fetch();


        // listView = (ListView) findViewById(R.id.list_view);
        recyclerView = findViewById(R.id.list_view);
        noRecords = findViewById(R.id.empty);

        //  listView.setEmptyView(findViewById(R.id.empty));

        //  adapter = new SimpleCursorAdapter(this, R.layout.activity_view_record, cursor, from, to, 0);
        //  adapter.notifyDataSetChanged();

        //  listView.setAdapter(adapter);

        // OnCLickListiner For List Items
//        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long viewId) {
//                TextView idTextView = (TextView) view.findViewById(R.id.id);
//                TextView titleTextView = (TextView) view.findViewById(R.id.title);
//                TextView descTextView = (TextView) view.findViewById(R.id.desc);
//
//                String id = idTextView.getText().toString();
//                String title = titleTextView.getText().toString();
//                String desc = descTextView.getText().toString();
//
//                Intent modify_intent = new Intent(getApplicationContext(), ModifyCountryActivity.class);
//                modify_intent.putExtra("title", title);
//                modify_intent.putExtra("desc", desc);
//                modify_intent.putExtra("id", id);
//
//                startActivity(modify_intent);
//            }
//        });
        initiRecycleView();
    }

    private void initiRecycleView() {
        countryAdapter = new CountryAdapter(this) {
            @Override
            public void click(CountryDataEntity item, View view) {

                TextView idTextView = view.findViewById(R.id.id);
                TextView titleTextView = view.findViewById(R.id.title);
                TextView descTextView = view.findViewById(R.id.desc);

                String id = idTextView.getText().toString();
                String title = titleTextView.getText().toString();
                String desc = descTextView.getText().toString();

                Intent modify_intent = new Intent(getApplicationContext(), ModifyCountryActivity.class);
                modify_intent.putExtra("title", title);
                modify_intent.putExtra("desc", desc);
                modify_intent.putExtra("id", id);

                startActivity(modify_intent);
            }
        };
        LinearLayoutManager gridLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(countryAdapter);
        getFromRoom();
      //  countryAdapter.setArray(getFromRoom());
//        if (CollectionUtils.isEmpty(getFromRoom())) {
//            noRecords.setVisibility(View.VISIBLE);
//        } else {
//            noRecords.setVisibility(View.GONE);
//        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.add_record) {

            Intent add_mem = new Intent(this, AddCountryActivity.class);
            startActivity(add_mem);

        }
        return super.onOptionsItemSelected(item);
    }

    private List <CountryDataEntity> getList() {
        List<CountryDataEntity> mArrayList = new LinkedList<>();
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            // The Cursor is now set to the right position
            mArrayList.add(new CountryDataEntity(cursor.getString(cursor.getColumnIndex(DatabaseHelper.SUBJECT)), cursor.getString(cursor.getColumnIndex(DatabaseHelper.DESC)), cursor.getLong(cursor.getColumnIndex(DatabaseHelper._ID))));
        }
        return mArrayList;
    }
    @SuppressLint("CheckResult")
    private void getFromRoom() {

        Observable.fromCallable(() -> {
            List<CountryDataEntity> locationEntities = RoomClient.getInstance(this).getAppDatabase().countryDao().getData();
            countryAdapter.setArray(locationEntities);
            return locationEntities;

        }).subscribeOn(Schedulers.newThread())
                .subscribe(aBoolean -> {
                    Log.e("TAG_DATABASE", "result : " + aBoolean);
                }, throwable -> {
                    Log.e("TAG_DATABASE", "error : ", throwable);
                });

    }

}