package com.journaldev.view;

/**
 * Created by anupamchugh on 19/10/15.
 */
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.journaldev.room.RoomClient;
import com.journaldev.room.model.CountryDataEntity;
import com.journaldev.sqlite.DBManager;
import com.journaldev.sqlite.R;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class ModifyCountryActivity extends Activity implements OnClickListener {

    private EditText titleText;
    private Button updateBtn, deleteBtn;
    private EditText descText;

    private long _id;

    private DBManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTitle("Modify Record");

        setContentView(R.layout.activity_modify_record);

        dbManager = new DBManager(this);
        dbManager.open();

        titleText = (EditText) findViewById(R.id.subject_edittext);
        descText = (EditText) findViewById(R.id.description_edittext);

        updateBtn = (Button) findViewById(R.id.btn_update);
        deleteBtn = (Button) findViewById(R.id.btn_delete);

        Intent intent = getIntent();
        String id = intent.getStringExtra("id");
        String name = intent.getStringExtra("title");
        String desc = intent.getStringExtra("desc");

        _id = Long.parseLong(id);

        titleText.setText(name);
        descText.setText(desc);

        updateBtn.setOnClickListener(this);
        deleteBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_update:
                String title = titleText.getText().toString();
                String desc = descText.getText().toString();

              // dbManager.update(_id, title, desc);

                update(title, desc,_id);
                this.returnHome();
                break;

            case R.id.btn_delete:
             //  dbManager.delete(_id);
                deleteCountry(titleText.getText().toString(),  descText.getText().toString(),_id);
                this.returnHome();
                break;
        }
    }

    public void returnHome() {
        Intent home_intent = new Intent(getApplicationContext(), CountryListActivity.class)
                .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(home_intent);
    }
    @SuppressLint("CheckResult")
    public void deleteCountry(String name, String desc,long _id) {

        Observable.fromCallable(() -> {
            RoomClient.getInstance(this).getAppDatabase().countryDao().delete(new CountryDataEntity( name,desc,_id));
            return true;
        }).subscribeOn(Schedulers.newThread())
                .subscribe(aBoolean -> {
                    Log.e("TAG_DATABASE", "result : " + aBoolean);
                }, throwable -> {
                    Log.e("TAG_DATABASE", "error : ", throwable);
                });



    }
    @SuppressLint("CheckResult")
    public void update(String title, String desc,long _id) {
        Observable.fromCallable(() -> {
            RoomClient.getInstance(this).getAppDatabase().countryDao().update(new CountryDataEntity( title, desc,_id));
            return true;
        }).subscribeOn(Schedulers.newThread())
                .subscribe(aBoolean -> {
                    Log.e("TAG_DATABASE", "result : " + aBoolean);
                }, throwable -> {
                    Log.e("TAG_DATABASE", "error : ", throwable);
                });



    }

}
